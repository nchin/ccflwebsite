// Call plugins
var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var config = require('./config.json');

var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');
var del = require('del');
var runSequence = require('run-sequence');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var sassGlob = require('gulp-sass-glob');
var svgSprite = require('gulp-svg-sprite');
var spritesmith = require('gulp.spritesmith');
var jshint = require('gulp-jshint');
var jscs = require('gulp-jscs');


// Development Tasks
// -----------------


// Custom plumber function that emits the end event with the plumber plugin
function customPlumber(errTitle) {
	return plumber({
		errorHandler: notify.onError({
			// Customizing error title
			title: errTitle || "Error running Gulp",
			message: "Error: <%=error.message %>",
			sound: "Glass"
		})
	});
}

// Sass task
gulp.task('sass', function(){
	return gulp.src(config.css.src)// Gets all files ending with .scss in src/scss and children dirs
		.pipe(sassGlob())

		// Checks for errors all plugins
		.pipe(customPlumber('Error Running Sass'))

		// initialize the sourcemaps plugin
		.pipe(sourcemaps.init())

		// Compiles Sass to CSS with gulp-sass
		.pipe(sass({
			//includePaths: ['./node_modules/bootstrap-sass/assets/stylesheets'], 	// Array of paths that libsass looks to resolve @import declarations.
			outputStyle: 'nested', // Output format for styles.
			precision: 3		// Number of decimal points.
		}))

		// rus produced CSS through autoprefixer
		.pipe(autoprefixer('last 2 versions', '> 1%', 'ie 9', 'ie 10'))

		// writing sourcemaps
		.pipe(sourcemaps.write('./'))

		.pipe(gulp.dest(config.css.dest)) // Outputs it in the css folder

		// Tells BrowserSync to reload files task is done
		// .pipe(browserSync.reload({ stream: true }));
});

// SVG Config
// var svgConfig = {
//   mode: {
//     symbol: { // symbol mode to build the SVG
//       render: {
//         css: false, // CSS output option for icon sizing
//         scss: false // SCSS output option for icon sizing
//       },
//       dest: '.', // destination folder
//       prefix: '.svg--%s', // BEM-style prefix if styles rendered
//       sprite: 'sprite.svg', //generated sprite name
//       example: true // Build a sample page, please!
//     }
//   }
// };

// Create SVG Sprite
// gulp.task('svg-sprite', function() {
//     return gulp.src('**/*.svg', {cwd: 'src/svg'})
// 		.pipe(customPlumber('Error Running SVG Srpite'))
// 	    .pipe(svgSprite(svgConfig))
// 	    .pipe(gulp.dest('src/svg-sprite'))
// 	 //    .pipe(browserSync.reload({
// 		// 	stream: true
// 		// }));
// });



// Watch task
gulp.task('watch', function (){
	gulp.watch(config.css.src, ['sass']);
  	// gulp.watch(config.images.src, ['images']);
	// gulp.watch('src/svg/**/*.svg', ['svg-sprite']);
	// gulp.watch('src/svg-sprite/**/*.svg', browserSync.reload);
	// gulp.watch(config.html.src, browserSync.reload);
 //  	gulp.watch(config.js.src, browserSync.reload);

});

// Copy sprites to dist folder
// gulp.task('sprite-copy', function() {
//   return gulp.src('src/svg-sprite/sprite.svg')
//     .pipe(gulp.dest('dist/svg-sprite'));
// });

// Copy documents to dist folder
// gulp.task('doc', function() {
//   return gulp.src('src/doc/**/*')
//     .pipe(gulp.dest('dist/doc'));
// });

// Deleting files that are no longer used
// gulp.task('clean', function() {
//   return del.sync('dist').then(function(cb) {
//     return cache.clearAll(cb);
//   });
// });

// gulp.task('clean:dist', function(){
// 	return del.sync(['dist/**/*', '!dist/images', '!dist/images/**/*']);
// });

// Clear caches off local system
// gulp.task('cache:clear', function (callback) {
// 	return cache.clearAll(callback);
// });


// Build Sequences
// ---------------
// Consolidated dev phase task
gulp.task('default', function(callback) {
	runSequence(
	// 	'clean:dev',
	// 	'sprites',
		['sass'],
		['watch'],
		callback
	);
});

// gulp.task('build', function(callback) {
// 	runSequence(
// 		'clean:dist',
// 		'sass',
// 		['useref', 'images', 'fonts:dist', 'sprite-copy', 'doc'],
// 		callback
// 	);
// });